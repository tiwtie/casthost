#!/usr/bin/env bash

if [ "$#" != "2" ]; then
  printf "You must provide two arguments: stage (stage1 || stage2) and destination path."
fi

STAGE=$1
DEST=$2

function chmodcopy() {
  FILE=$1
  chmod -R g+w $FILE
  chgrp -R www-data $FILE
  cp -r --preserve=mode $FILE $DEST/
}

function stage1() {
  # The basics:
  chmodcopy yarn.lock
  chmodcopy package.json
  # Yarn 3 + pnp files:
  chmodcopy .yarn
  chmodcopy tsconfig.json
}

function stage2() {
  chmodcopy public
  rm -rf $DEST/.next
  chmodcopy .next
  chmodcopy next.config.js
  chmodcopy src
  chmodcopy styles
}

if [ "$STAGE" == "stage1" ]; then
  stage1
elif [ "$STAGE" == "stage2" ]; then
  stage2
else
  printf "Incorrect stage specified: %s. Allowed values: stage1, stage2\n"
  exit -1
fi