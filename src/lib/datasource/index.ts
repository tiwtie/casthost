import { readFile, readdir } from 'fs/promises'
import { ChannelData, EpisodeJson, SeasonData } from '../entity'
import { sep } from 'path'

/**
 * TODO: This file abstracts calls to FS without consolidating them into a class. 
 * This is easy, but also a bit annoying since we can't really mock these calls.
 * 
 * The reasoning behind this is that I haven't figured out a neat way to do dependency
 * injection in Next.JS yet and I don't want to instantiate this class every time I need
 * a call from this file. While this service isn't particularly high traffic, I'd like
 * to add in-memory cache at some point and that doesn't work if the cache only lives
 * in the scope of a single request. 
 * 
 * If all else fails I can still store the cache in Redis.
 */

export const rootPath = `${process.cwd()}${sep}data`

/**
 * Get a podcast from disk.
 */
export const getChannelData = async (pid: string): Promise<ChannelData> => {
  return JSON.parse(
    await readFile(`${rootPath}${sep}podcasts${sep}${pid}${sep}meta.json`, 'utf8')
  )
}

/**
 * Get a specific episode from a podcast.
 */
export const getEpisodeData = async (
  pid: string,
  eid: string
): Promise<EpisodeJson> => {
  const fileContents = await readFile(
    `${rootPath}${sep}podcasts${sep}${pid}${sep}episodes${sep}${eid}${sep}meta.json`,
    'utf8'
  )
  return {
    ...JSON.parse(fileContents),
    link: `${process.env.SITE_HOST}${sep}podcasts${sep}${pid}${sep}${eid}${sep}player`,
    imageUrl: `${process.env.SITE_HOST}${sep}api${sep}podcast${sep}2${sep}${eid}${sep}image.jpg`,
  }
}

/**
 * Get all the ids of the podcasts in this instance of Casthost.
 */
export const getPodcastIds = async (): Promise<string[]> => {
  return readdir(`${rootPath}/podcasts`)
}

/**
 * Get all the episode ids of the specified podcast. 
 */
export const getEpisodeIds = async (pid: string): Promise<string[]> => {
  return readdir(`${rootPath}/podcasts/${pid}/episodes`)
}

/**
 * Reads and returns an image from disk for the episode of a podcast with the specified ids.
 */
export const getEpisodeImage = async (
  pid: string,
  eid: string
): Promise<Buffer> => {
  return readFile(`${rootPath}/podcasts/${pid}/episodes/${eid}/image.jpg`)
}

/**
 * Reads and returns an image from disk for the podcast with the specified id
 */
export const getPodcastImage = async (pid: string): Promise<Buffer> => {
  return readFile(`${rootPath}/podcasts/${pid}/image.jpg`)
}

/**
 * Retrieves a season "from disk". Since this mechanism hasn't been implemented yet
 * we will just "dynamically" return values in Dutch for Museumcast.
 */
export const getSeasonData = async (pid: string, sid: string): Promise<SeasonData> => {
  if (pid === '2' && (sid === '1' || sid === '2')) { // Museumcast
    return {
      name: `Seizoen ${sid}`,
      index: parseInt(sid, 10),
    }
  }
  return {
    name: `Season ${sid}`,
    index: parseInt(sid, 10),
  }
}