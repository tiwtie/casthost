export class SymbolReplacer {
  static sanitize(str: string): string {
    return new String(str).replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/'/g, '&apos;')
      .replace(/"/g, '&quot;')
      .replace(/©/g, '&#xA9;')
      .replace(/℗/g, '&#x2117;')
      .replace(/™/g, '&#x2122;')
  }
}