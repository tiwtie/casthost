import { SymbolReplacer } from './symbolreplacer'

describe('symbolreplacer', () => {

  it('should contain replaced output', () => {
    const output = SymbolReplacer.sanitize('A&B > C&D \'one\' < "two" © ℗ ™')
    expect(output).toEqual('A&amp;B &gt; C&amp;D &apos;one&apos; &lt; &quot;two&quot; &#xA9; &#x2117; &#x2122;')
  })

})