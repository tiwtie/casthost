import { XMLBuilder } from 'xmlbuilder2/lib/interfaces'

export interface Serializable {
    toXml: () => XMLBuilder
}