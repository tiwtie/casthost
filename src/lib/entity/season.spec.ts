import { Season } from './season'

describe('season', () => {
  const e = new Season('Podcast v1', 1)
  const xmlString = e.toXml().end({ prettyPrint: true })
  it('format all', () => {
    expect(xmlString).toContain('<podcast:season name="Podcast v1">1</podcast:season>')
  })
})