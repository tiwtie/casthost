import { fragment } from 'xmlbuilder2'
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces'
import { Serializable } from '../util/serializable'

export interface PersonData {
  name: string
  website?: string
  role?: string
  imageUrl?: string
}

export class Person implements Serializable {
  data: PersonData

  constructor(data: PersonData) {
    this.data = data
  }

  toXml(): XMLBuilder {
    const root = fragment().ele('podcast:person')
    root.txt(this.data.name)
    this.data.website && root.att('href', this.data.website)
    this.data.role && root.att('role', this.data.role)
    this.data.imageUrl && root.att('img', this.data.imageUrl)
    return root
  }
}
