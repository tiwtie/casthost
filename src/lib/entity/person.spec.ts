import { Person } from './person'

describe('person', () => {
  const e = new Person({
    name: 'Alberto Potato',
    website: 'http://alber.to/potato',
    role: 'guest',
    imageUrl: 'http://c.img',
  })
  const xmlString = e.toXml().end({ prettyPrint: true })
  it('format all', () => {
    expect(xmlString).toContain(
      '<podcast:person href="http://alber.to/potato" role="guest" img="http://c.img">'
    )
  })
})
