import { fragment } from 'xmlbuilder2'
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces'
import { Episode } from './episode'
import { Serializable } from '../util/serializable'

export interface ChannelData {
  title: string
  link: string
  language: string
  copyright: string
  subtitle: string
  summary: string
  ownerName: string
  ownerEmail: string
  image: string
  categories: string[]
  explicit: boolean
  episodes: Episode[]
}

export type ChannelInputData = Omit<ChannelData, 'image'>

export class Channel implements Serializable {
  data: ChannelData
  episodes?: Episode[]

  constructor(data: ChannelInputData, episodes?: Episode[]) {
    this.data = {
      ...data,
      image: `${process.env.SITE_HOST}/api/podcast/2/image.jpg`,
    }
    this.episodes = episodes
  }

  toXml(): XMLBuilder {
    const root = fragment().ele('channel')

    const setKv = (elemName: string, elemValue: string) => {
      const newElement = root.ele(elemName)
      newElement.txt(elemValue)
    }

    const setKe = (
      elemName: string,
      attrMap: { [attrKey: string]: string }
    ) => {
      root.ele(elemName).att(attrMap)
    }

    setKv('title', this.data.title)
    setKv('link', this.data.link)
    setKv('language', this.data.language)
    setKv('copyright', this.data.copyright)
    setKv('itunes:subtitle', this.data.subtitle)
    setKv('itunes:author', this.data.ownerName)
    setKv('itunes:summary', this.data.summary)
    setKv('itunes:type', 'serial')
    setKv('description', this.data.summary)

    const ownerElem = root.ele('itunes:owner')
    ownerElem.ele('itunes:name').txt(this.data.ownerName)
    ownerElem.ele('itunes:email').txt(this.data.ownerEmail)

    setKe('itunes:image', {
      href: this.data.image,
    })
    this.data.categories.map((c) => {
      setKe('itunes:category', {
        text: c,
      })
    })

    setKv('itunes:explicit', this.data.explicit ? 'yes' : 'no')

    this.episodes &&
      this.episodes
        .sort((e1, e2) => e2.data.pubDate.getTime() - e1.data.pubDate.getTime())
        .filter((episode) => episode.data.pubDate < new Date())
        .forEach((episode) => root.import(episode.toXml()))

    return root
  }
}
