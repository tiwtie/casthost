import { EnclosureData } from './enclosure'
import { Episode, EpisodeJson } from './episode'
import { Season } from './season'

describe('episode', () => {
  const s = new Season('Hello world', 1)
  const enc: EnclosureData = {
    url: 'https://a.mp3',
    length: 1234567,
    type: 'audio/mpeg',
    bitrate: 128000,
  }
  const episodeData: EpisodeJson = {
    title: 'a',
    description: 'b',
    link: 'http://c/',
    episode: 1,
    people: [],
    season: s,
    enclosure: enc,
    altEnclosures: [],
    imageUrl: 'abc',
    pubDate: new Date().toISOString(),
    duration: '12:34:56',
    block: false,
  }
  const e = new Episode(episodeData)
  const xmlString = e.toXml().end({ prettyPrint: true })

  it.each([
    ['title', '<title>a</title>'],
    ['description', '<description><![CDATA[<p>b</p>]]></description>'],
    ['itunes:summary', '<itunes:summary><![CDATA[<p>b</p>]]></itunes:summary>'],
    [
      'content:encoded',
      '<content:encoded><![CDATA[<p>b</p>]]></content:encoded>',
    ],
    ['link', '<link>http://c/</link>'],
    [
      'enclosure',
      '<enclosure url="https://dts.podtrac.com/redirect.mp3/a.mp3" length="1234567" type="audio/mpeg"',
    ],
  ])('serializes $fieldName', (fieldName, expectedValue) => {
    expect(xmlString).toContain(expectedValue)
  })

  it('doesn\'t contain the itunes block element if blocked is not specified', () => {
    expect(xmlString).not.toContain('<itunes:block>yes</itunes:block>')
  })

  it('serializes people', () => {
    const boris = {
      name: 'Boris',
      website: 'https://boris.dev',
      role: 'master',
      imageUrl: 'https://boris.dev/mugshot.jpg',
    }
    
    const ep = new Episode({
      ...episodeData,
      people: [boris],
    })
    const epXml = ep.toXml().end({ prettyPrint: true })
    expect(epXml).toContain('>Boris<')
  })

  it('serializes the block element if block is true', () => {
    const ep = new Episode({
      ...episodeData,
      block: true,
    })
    const epXml = ep.toXml().end({ prettyPrint: true })
    expect(epXml).toContain('<itunes:block>yes</itunes:block>')
  })
})
