import { XMLBuilder } from 'xmlbuilder2/lib/interfaces'
import { fragment } from 'xmlbuilder2'
import { Serializable } from '../util/serializable'

export interface EnclosureData {
  url: string
  length: number
  type: string
  bitrate: number
}

export class Enclosure implements Serializable {
  data: EnclosureData

  constructor(data: EnclosureData) {
    this.data = data
  }

  toXml(): XMLBuilder {
    const root = fragment().ele('enclosure')
    root.att(
      'url',
      `https://dts.podtrac.com/redirect.mp3/${this.data.url.replace(
        'https://',
        ''
      )}`
    )
    root.att('length', `${this.data.length}`)
    root.att('type', this.data.type)
    root.att('bitrate', `${this.data.bitrate}`)
    return root
  }
}

interface AlternateEnclosureSourceObj {
  uri: string
}

export class AlternateEnclosurePodcastSource implements Serializable {
  uri: string
  constructor(uri: string) {
    this.uri = uri
  }

  toXml(): XMLBuilder {
    const root = fragment().ele('podcast:source')
    root.att(
      'uri',
      `https://dts.podtrac.com/redirect.mp3/${this.uri.replace('https://', '')}`
    )
    return root
  }

  static fromObject(
    obj: AlternateEnclosureSourceObj
  ): AlternateEnclosurePodcastSource {
    return new AlternateEnclosurePodcastSource(obj.uri)
  }
}

interface AlternateEnclosureObj {
  title: string
  length: number
  bitrate: number
  type: string
  sources: AlternateEnclosurePodcastSource[]
  isDefault: boolean
}

export class AlternateEnclosure implements Serializable {
  title: string
  length: number
  bitrate: number
  type: string
  sources: AlternateEnclosurePodcastSource[]
  isDefault: boolean

  constructor(
    title: string,
    length: number,
    bitrate: number,
    type: string,
    sources: AlternateEnclosurePodcastSource[],
    isDefault = false
  ) {
    this.type = type
    this.length = length
    this.bitrate = bitrate
    this.title = title
    this.sources = sources
    this.isDefault = isDefault
  }

  static fromEnclosure(enclosure: EnclosureData): AlternateEnclosure {
    return new AlternateEnclosure(
      'Standard',
      enclosure.length,
      enclosure.bitrate,
      enclosure.type,
      [new AlternateEnclosurePodcastSource(enclosure.url)],
      true
    )
  }

  toXml(): XMLBuilder {
    const root = fragment().ele('podcast:alternateEnclosure')
    root.att('type', this.type)
    root.att('length', `${this.length}`)
    root.att('bitrate', `${this.bitrate}`)
    root.att('title', this.title)
    if (this.isDefault) root.att('default', 'true')

    this.sources.forEach((s) => root.import(s.toXml()))
    return root
  }

  static fromObject(obj: AlternateEnclosureObj): AlternateEnclosure {
    const sources = obj.sources
    return new AlternateEnclosure(
      obj.title,
      obj.length,
      obj.bitrate,
      obj.type,
      sources.map((source) =>
        AlternateEnclosurePodcastSource.fromObject(source)
      ),
      obj.isDefault
    )
  }
}
