import { fragment } from 'xmlbuilder2'
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces'
import { Serializable } from '../util/serializable'

export interface SeasonData {
  name: string
  index: number
}

export class Season implements Serializable {
  name: string
  index: number

  constructor(name: string, index: number) {
    this.name = name
    this.index = index
  }

  toXml(): XMLBuilder {
    const root = fragment().ele('podcast:season')
    root.att('name', this.name)
    root.txt(`${this.index}`)
    return root
  }

  static fromObject(obj: SeasonData): Season {
    return new Season(obj.name, obj.index)
  }
}
