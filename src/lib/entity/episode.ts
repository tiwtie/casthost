import { fragment } from 'xmlbuilder2'
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces'
import { Person, PersonData } from './person'
import { Season, SeasonData } from './season'
import { AlternateEnclosure, Enclosure, EnclosureData } from './enclosure'
import { Serializable } from '../util/serializable'

/**
 * Representation of an episode that is serializable to Json
 */
export type EpisodeJson = {
  title: string
  description: string
  link: string
  episode: number
  season: SeasonData
  people: PersonData[]
  enclosure: EnclosureData
  altEnclosures: AlternateEnclosure[]
  imageUrl: string
  pubDate: string
  duration: string
  block: boolean
}

/**
 * Enriched representation of an episode where the serialized child properties have
 * also been deserialized using their respective constructors. 
 */
export type EpisodeData = Omit<EpisodeJson, 'season' | 'people' | 'pubDate'> & {
  season: Season
  people: Person[]
  pubDate: Date
}

/**
 * The highest level representation of a podcast episode. This class encapsulates
 * the EpisodeData type and allows serialization to XML. 
 */
export class Episode implements Serializable {
  data: EpisodeData

  constructor(data: EpisodeJson) {
    this.data = {
      ...data,
      season: new Season(data.season.name, data.season.index),
      pubDate: new Date(data.pubDate),
      people: data.people.map((person) => new Person(person))
    }
  }

  toXml(): XMLBuilder {
    const root = fragment().ele('item')
    root.ele('title').txt(this.data.title)
    root.ele('itunes:title').txt(this.data.title)
    root.ele('itunes:season').txt(`${this.data.season.index}`)
    root.ele('description').dat(`<p>${this.data.description}</p>`)
    root.ele('itunes:summary').dat(`<p>${this.data.description}</p>`)
    root.ele('content:encoded').dat(`<p>${this.data.description}</p>`)
    root.ele('link').txt(this.data.link)
    root.ele('guid').txt(`museumcast:1:${this.data.episode}`)
    root.ele('itunes:image').att('href', this.data.imageUrl)
    root.ele('itunes:duration').txt(this.data.duration)
    root.ele('pubDate').txt(this.data.pubDate.toUTCString())
    root.ele('itunes:episodeType').txt('full')
    //author
    //podcast:images
    root.import(this.data.season.toXml())
    root.ele('podcast:episode').txt(`${this.data.episode}`)
    root.ele('itunes:episode').txt(`${this.data.episode}`)
    root.ele('itunes:explicit').txt(`${true}`)
    this.data.people.forEach((p) => root.import(p.toXml()))
    root.import(new Enclosure(this.data.enclosure).toXml())
    root.import(AlternateEnclosure.fromEnclosure(this.data.enclosure).toXml())
    this.data.altEnclosures.forEach((enclosure) =>
      root.import(enclosure.toXml())
    )
    if (this.data.block) {
      root.ele('itunes:block').txt('yes')
    }
    return root
  }
}
