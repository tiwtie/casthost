import { Channel } from './channel'
import { Feed } from './feed'

describe('feed', () => {
  const a = new Channel({
    title: 'a',
    link: 'b',
    language: 'en',
    copyright: 'd',
    subtitle: 'e',
    summary: 'g',
    ownerName: 'h',
    ownerEmail: 'i',
    categories: ['k'],
    explicit: false,
    episodes: [],
  })
  const f = new Feed(a)
  const xmlOutputLines = f.toXmlString().split('\n')

  it('should contain a root with version 1.0 and encoding utf-8', () => {
    expect(xmlOutputLines[0]).toEqual('<?xml version="1.0" encoding="UTF-8"?>')
  })

  it('should contain the itunes rss element on the second line', () => {
    expect(xmlOutputLines[1]).toEqual(
      '<rss version="2.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:podcast="https://github.com/Podcastindex-org/podcast-namespace/blob/main/docs/1.0.md" xmlns:googleplay="http://www.google.com/schemas/play-podcasts/1.0/">'
    )
  })

  it('should contain the channel tag on the third line', () => {
    expect(xmlOutputLines[2]).toEqual('  <channel>')
  })
})
