import { create } from 'xmlbuilder2'
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces'
import { Channel } from './channel'
import { Serializable } from '../util/serializable'

export class Feed implements Serializable {
  channel: Channel

  constructor(channel: Channel) {
    this.channel = channel
  }

  toXmlString(): string {
    return this.toXml().end({ prettyPrint: true })
  }

  toXml(): XMLBuilder {
    const root = create({
      version: '1.0',
      encoding: 'UTF-8',
    })
    return root
      .ele('rss', {
        version: '2.0',
        'xmlns:itunes': 'http://www.itunes.com/dtds/podcast-1.0.dtd',
        'xmlns:content': 'http://purl.org/rss/1.0/modules/content/',
        'xmlns:podcast':
          'https://github.com/Podcastindex-org/podcast-namespace/blob/main/docs/1.0.md',
        'xmlns:googleplay': 'http://www.google.com/schemas/play-podcasts/1.0/',
      })
      .import(this.channel.toXml())
  }
}
