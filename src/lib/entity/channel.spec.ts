import { Channel } from './channel'
import { EnclosureData } from './enclosure'
import { Episode, EpisodeJson } from './episode'
import { Season } from './season'

describe('channel', () => {
  const s = new Season('Hello world', 1)
  
  const enc: EnclosureData = {
    url: 'https://a.mp3',
    length: 1234567,
    type: 'audio/mpeg',
    bitrate: 128000,
  }

  const episodeData: EpisodeJson = {
    title: 'a',
    description: 'b',
    link: 'http://c/',
    episode: 1,
    people: [],
    season: s,
    enclosure: enc,
    altEnclosures: [],
    imageUrl: 'abc',
    pubDate: new Date().toISOString(),
    duration: '12:34:56',
    block: false,
  }
  const episode = new Episode(episodeData)

  const channel = new Channel({
    title: 'a',
    link: 'b',
    language: 'en',
    copyright: 'd',
    subtitle: 'e',
    summary: 'g',
    ownerName: 'h',
    ownerEmail: 'i',
    categories: ['k'],
    explicit: false,
    episodes: [episode],
  })

  const outputXml = channel.toXml().end({ prettyPrint: true })

  it('should serialize the title correctly', () => {
    expect(outputXml).toContain('<title>a</title>')
  })

  it('should serialize the link correctly', () => {
    expect(outputXml).toContain('<link>b</link>')
  })

  it('should serialize the language correctly', () => {
    expect(outputXml).toContain('<language>en</language>')
  })

  it('should serialize the copyright holder correctly', () => {
    expect(outputXml).toContain('<language>en</language>')
  })

  it('should serialize the itunes:owner correctly', () => {
    expect(outputXml).toContain(`<itunes:owner>
    <itunes:name>h</itunes:name>
    <itunes:email>i</itunes:email>
  </itunes:owner>`)
  })

  it('should serialize the itunes:subtitle correctly', () => {
    expect(outputXml).toContain('<itunes:subtitle>e</itunes:subtitle>')
  })

  it('should serialize the itunes:author correctly', () => {
    expect(outputXml).toContain('<itunes:author>h</itunes:author>')
  })

  it('should serialize the itunes:image correctly', () => {
    expect(outputXml).toContain(
      '<itunes:image href="undefined/api/podcast/2/image.jpg"/>'
    )
  })

  it('should serialize the itunes:category correctly', () => {
    expect(outputXml).toContain('<itunes:category text="k"/>')
  })

  it('should serialize the itunes:explicit into a no if false', () => {
    expect(outputXml).toContain('<itunes:explicit>no</itunes:explicit>')
  })

  it('should serialize a channel opening element on the second line', () => {
    expect(outputXml.split('\n')[0]).toContain('<channel>')
  })

  it('should serialize a channel closing element on the last line', () => {
    const lines = outputXml.split('\n')
    expect(lines[lines.length - 1]).toContain('</channel>')
  })
})
