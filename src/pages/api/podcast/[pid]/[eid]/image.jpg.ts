import { getEpisodeImage } from 'lib/datasource'
import type { NextApiRequest, NextApiResponse } from 'next'

export default async function EpisodeImage(
  req: NextApiRequest,
  res: NextApiResponse<Buffer>
) {
  res.setHeader('Content-Type', 'image/jpg')
  res.send(
    await getEpisodeImage(req.query.pid as string, req.query.eid as string)
  )
}
