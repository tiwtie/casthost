// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { Episode } from 'lib/entity'
import { getEpisodeData } from 'lib/datasource'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Episode>
) {
  const episodeFromDisk: Episode = new Episode(
    await getEpisodeData(req.query.pid as string, req.query.eid as string)
  )
  res.status(200).json(episodeFromDisk)
}
