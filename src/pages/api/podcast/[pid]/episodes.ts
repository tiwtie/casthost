import type { NextApiRequest, NextApiResponse } from 'next'
import { EpisodeJson } from 'lib/entity'
import { getEpisodeData, getEpisodeIds } from 'lib/datasource'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<EpisodeJson[]>
) {
  const pid = req.query.pid as string
  const episodeIds = await getEpisodeIds(pid)
  const episodes: EpisodeJson[] = await Promise.all(
    episodeIds.map((eid) => getEpisodeData(pid, eid))
  )
  res.status(200).json(episodes)
}
