import { getPodcastImage } from 'lib/datasource'
import type { NextApiRequest, NextApiResponse } from 'next'

export default function PodcastImage(
  req: NextApiRequest,
  res: NextApiResponse
) {
  res.setHeader('Content-Type', 'image/jpg')
  res.send(getPodcastImage(req.query.pid as string))
}
