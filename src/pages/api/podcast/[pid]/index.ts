// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { Channel } from 'lib/entity'
import { getChannelData } from 'lib/datasource'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Channel>
) {
  const podcastFromDisk: Channel = new Channel(
    await getChannelData(req.query.pid as string)
  )
  res.status(200).json(podcastFromDisk)
}
