// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { Channel, Episode, Feed } from 'lib/entity'
import { getChannelData, getEpisodeData, getEpisodeIds } from 'lib/datasource'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Feed>
) {
  const pid = req.query.pid as string
  const episodeIds = await getEpisodeIds(pid)
  const episodes = await Promise.all(
    episodeIds.sort((a, b) => { return parseInt(b, 10) - parseInt(a, 10)}).map(async (eid) => new Episode(await getEpisodeData(pid, eid)))
  )
  const channelFromDisk = new Channel(await getChannelData(pid), episodes)

  res.setHeader('Content-Type', 'text/xml')
  res
    .status(200)
    .write(new Feed(channelFromDisk).toXml().end({ prettyPrint: true }))
  res.end()
}
