import type { NextPage } from 'next'
import Head from 'next/head'
import styles from 'styles/Home.module.css'
import styled from 'styled-components'
import getConfig from 'next/config'
import { PodcastPlatformIcon } from '../components'

const PodcastPlatformCollection = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  max-width: 20em;
  justify-content: space-evenly;
`

const Logo = styled.img`
  border-radius: 1em;
`

const Home: NextPage = () => {
  const config = getConfig()

  return (
    <div className={styles.container}>
      <Head>
        <title>Museumcast</title>
        <link rel="icon" href="/favicon.png" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
        <link rel="manifest" href="/site.webmanifest"></link>
        <link type="application/rss+xml" rel="alternate" title="Museumcast Podcast" href={`${config.publicRuntimeConfig.SITE_HOST}/api/feed/2`} />
      </Head>

      <main className={styles.main}>
        <Logo src="/favicon.png" alt="Museumcast" />

        <p className={styles.description}>
          De <a href="/podcasts/2">eerste afleveringen</a> staan live! <br/>
          Luister direct op onze <a href="/podcasts/2">web-player</a> of kies hier onder je favoriete podcast platform.
        </p>

        <PodcastPlatformCollection>
          <PodcastPlatformIcon
            link="itmss://podcasts.apple.com/us/podcast/museumcast/id1602024515?app=podcast"
            name="Apple"
            iconPath="/assets/apple-podcast.png"
          />
          <PodcastPlatformIcon
            link="https://open.spotify.com/show/2i8v1aaiJR8FCftFjOhThx"
            name="Spotify"
            iconPath="/assets/spotify-podcast.png"
          />
          <PodcastPlatformIcon
            link="https://podcasts.google.com/feed/aHR0cHM6Ly93d3cubXVzZXVtY2FzdC5ubC9hcGkvZmVlZC8y"
            name="Google"
            iconPath="/assets/google-podcast.png"
          />
          <PodcastPlatformIcon
            link={`${config.publicRuntimeConfig.SITE_HOST}/api/feed/2`}
            name="RSS"
            iconPath="/assets/rss-podcast.png"
          />
        </PodcastPlatformCollection>

      </main>

      <footer className={styles.footer}>
        <a
          href="https://tiwtie.xyz"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          ThisIsWhyTheInternetExists
        </a>
      </footer>
    </div>
  )
}

export default Home
