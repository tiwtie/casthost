import { NextPage } from 'next'
import Head from 'next/head'
import React from 'react'
import { ChannelData, EpisodeData } from 'lib/entity'
import styles from 'styles/Home.module.css'
import styled from 'styled-components'
import {
  getChannelData,
  getEpisodeData,
  getEpisodeIds,
  getPodcastIds,
} from 'lib/datasource'
import { HomeLink } from '../../../components'

const PodcastRow = styled.div`
  max-width: 30em;
  font-size: 1.2em;
  margin-top: 1em;
  display: flex;
  flex-direction: row;
  @media (max-width: 400px) {
    flex-direction: column;
    margin-top: 3em;
  }
  color: #000;
`

const PodcastImage = styled.img`
  width: 8em;
  height: 8em;
  border-radius: 8px;
  @media (max-width: 400px) {
    margin-left: auto;
    margin-right: auto;
    display: block;
  }
  @media (min-width: 400px) {
    margin-right: 1em;
  }
`

const PodcastInfo = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

const PodcastTitle = styled.span`
  font-size: 1.2em;
  font-weight: 500;
`

const TopInfoRow = styled.div`
  display: inline;
  @media (max-width: 400px) {
    text-align: center;
  }
`

const Description = styled.p`
  max-height: 100%;
  text-overflow: ellipsis;
  overflow: hidden;
  margin-bottom: 0;
  display: -webkit-box;
  -webkit-line-clamp: 4;
  line-clamp: 4;
  -webkit-box-orient: vertical;
  font-size: 0.9em;
  @media (max-width: 400px) {
    padding-left: 1em;
    padding-right: 1em;
  }
`

interface Props {
  channel: ChannelData
  episodes: EpisodeData[]
}

const PodcastSummary: NextPage<Props> = ({ channel, episodes }: Props) => {
  return (
    <div>
      <Head>
        <title>{`${channel.title}: Episode List`}</title>
      </Head>

      <main className={styles.main}>
        <HomeLink link='/' name={channel.title} />
        {episodes.map((e) => (
          <a href={e.link} key={`episode-${e.episode}`}>
            <PodcastRow>
              <PodcastImage src={e.imageUrl} />
              <PodcastInfo>
                <TopInfoRow>
                  <PodcastTitle>
                    {e.title}
                  </PodcastTitle>
                  <small>({e.duration})</small>
                </TopInfoRow>
                <Description
                  dangerouslySetInnerHTML={{ __html: e.description }}
                ></Description>
              </PodcastInfo>
            </PodcastRow>
          </a>
        ))}
      </main>
    </div>
  )
}

export default PodcastSummary

interface StaticPropsProps {
  params: {
    pid: string
  }
}

export async function getStaticProps(params: StaticPropsProps) {
  const pid = params.params.pid
  const episodeIds = await getEpisodeIds(pid)
  const episodes = await Promise.all(
    episodeIds.sort((a, b) => { return parseInt(b, 10) - parseInt(a, 10)}).map(async (eid) => await getEpisodeData(pid, eid))
  )
  const channelFromDisk: ChannelData = await getChannelData(pid)
  return {
    props: {
      channel: channelFromDisk,
      episodes: episodes,
    },
    revalidate: 120,
  }
}

interface PodcastPath {
  params: {
    pid: string
  }
}

async function getPodcastPaths(): Promise<PodcastPath[]> {
  try {
    return (await getPodcastIds()).map((p) => ({
      params: { pid: p },
    }))
  } catch {
    return []
  }
}

export async function getStaticPaths() {
  const podcastIds = await getPodcastPaths()
  return {
    paths: podcastIds,
    fallback: 'blocking',
  }
}
