import type { NextPage } from 'next'
import useSWR from 'swr'
import { useRouter } from 'next/router'
import { EpisodeData } from 'lib/entity'
import { HomeLink, PodcastPlayer } from '../../../../components'

const Player: NextPage = () => {
  const router = useRouter()
  const { pid, eid } = router.query

  if (!pid || !eid) {
    return (
      <>
        <h1>Loading</h1>
      </>
    )
  }

  const { data } = useSWR(
    `/api/podcast/${pid}/${eid}`,
    async (jan): Promise<EpisodeData> => {
      const response = await fetch(jan)
      return (await response.json()).data
    }
  )
  if (!data) {
    return (
      <>
        <h1>Loading</h1>
      </>
    )
  }
  return (
    <>
      {/* TODO: make sure `name` is taken from the API instead of hardcoded */}
      <HomeLink link='/' name='Museumcast'/>
      <PodcastPlayer data={data} />
    </>

  )
}


export default Player
