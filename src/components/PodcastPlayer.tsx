import React from 'react'
import styled from 'styled-components'
import AudioCard from 'audiocard'
import { EpisodeData } from 'lib/entity'

const StyledAudioCard = styled(AudioCard)`
  margin-top: 2em;
`

const PlayerFrame = styled.div`
  padding: 1em;
  max-width: 50em;
  margin-left: auto;
  margin-right: auto;
  background: #fefefe;
  border-radius: 1em;
`

export type PodcastPlayerProps = {
  data: EpisodeData
}

export class PodcastPlayer extends React.Component<PodcastPlayerProps> {
  constructor(props: PodcastPlayerProps) {
    super(props)
  }

  render() {
    return (
      <PlayerFrame>
        <StyledAudioCard 
          art={this.props.data.imageUrl}
          source={`https://dts.podtrac.com/redirect.mp3/stream.museumcast.nl/${this.props.data.episode}.mp3`}
          progressBarBackground={'#dedcbf'}
          title={`Episode ${this.props.data.episode}: ${this.props.data.title}`}
        />
        <h3>Samenvatting</h3>
        <p dangerouslySetInnerHTML={{ __html: this.props.data.description }}></p>
      </PlayerFrame>
    )
  }
}
