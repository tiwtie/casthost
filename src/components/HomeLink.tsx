import React from 'react'
import styled from 'styled-components'
import styles from 'styles/Home.module.css'

const StyledLink = styled.a`
  display: flex;
  flex-direction: column;
  font-size: 1.2em;
  align-items: center;
  font-weight: bold;
  text-decoration: none;
`

export type HomeLinkProps = {
  link: string
  name: string
}

export class HomeLink extends React.Component<HomeLinkProps> {
  constructor(props: HomeLinkProps) {
    super(props)
  }

  render() {
    return (
      <StyledLink href={this.props.link}>
        <h1 className={styles.title}>{this.props.name}</h1>
      </StyledLink>
    )
  }
}
