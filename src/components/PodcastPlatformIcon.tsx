import React from 'react'
import styled from 'styled-components'

const PodcastPlatform = styled.a`
  display: flex;
  flex-direction: column;
  font-size: 1.2em;
  align-items: center;
  font-weight: bold;
  text-decoration: none;
`

const PlatformLogo = styled.img`
  width: 3.2em;
  height: 3.2em;
`

export type PodcastPlatformIconProps = {
  link: string
  name: string
  iconPath: string
}

export class PodcastPlatformIcon extends React.Component<PodcastPlatformIconProps> {
  constructor(props: PodcastPlatformIconProps) {
    super(props)
  }

  render() {
    return (<PodcastPlatform href={this.props.link}>
      <PlatformLogo src={this.props.iconPath} />{this.props.name}
    </PodcastPlatform>)
  }
}
