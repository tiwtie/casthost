/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  publicRuntimeConfig: {
    SITE_HOST: process.env.SITE_HOST || 'https://www.museumcast.nl',
  },
}
